package fr.b3.epsi.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.IOException;

import fr.b3.epsi.model.App;

public class PeripheralController {

    @FXML
    TextField ID;

    @FXML
    private void openRootLayout() throws IOException {
        App.setRoot("rootLayout");
    }

    @FXML
    private void openCustomer() throws IOException {
        App.setRoot("customer");
    }

    @FXML
    private void openPeripheral() throws IOException {
        App.setRoot("peripheral");
    }

    @FXML
    public void createPeripheral() throws IOException {
        App.setRoot("createPeripheral");
    }

    @FXML
    public void createNetworkPeripheral() throws IOException {
        App.setRoot("createNetworkDevice");
    }

}

