package fr.b3.epsi.controller;
import java.io.IOException;

import fr.b3.epsi.model.App;
import javafx.fxml.FXML;

public class PrimaryController {

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
}

