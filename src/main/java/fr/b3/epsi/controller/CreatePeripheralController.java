package fr.b3.epsi.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.io.IOException;

import fr.b3.epsi.model.App;
import fr.b3.epsi.model.Peripheral;

public class CreatePeripheralController {

    @FXML
    TextField ID, Brand, Model, SerialNumber, BuyDate, InstallationDate, DeployDate;
    @FXML
    ChoiceBox<String> choiceType;

    @FXML
    private void openRootLayout() throws IOException {
        App.setRoot("rootLayout");
    }

    @FXML
    private void openCustomer() throws IOException {
        App.setRoot("customer");
    }

    @FXML
    private void openPeripheral() throws IOException {
        App.setRoot("peripheral");
    }

    @FXML
    public void initialize() {
        ObservableList<String> simpleType = FXCollections.observableArrayList();
        simpleType.addAll("Ecran", "Clavier", "Souris");
        choiceType.setItems(simpleType);
    }

    @FXML
    public void save() throws IOException {
        String type = choiceType.getValue();
        Peripheral peripheral = new Peripheral(
                ID.getText(),
                BuyDate.getText(),
                InstallationDate.getText(),
                Brand.getText(),
                SerialNumber.getText(),
                Model.getText(),
                type);
        App.setRoot("peripheral");
    }

}

