package fr.b3.epsi.controller;

import javafx.fxml.FXML;

import java.io.IOException;

import fr.b3.epsi.model.App;
import fr.b3.epsi.model.Peripheral;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class CreateNetworkDeviceController {
	
	@FXML TextField ID, Brand, Model, SerialNumber, BuyDate, InstallationDate, DeployDate, Ipv4, Dns, Netmask, Gateway, Url;
    @FXML TextArea Comments;
    @FXML RadioButton Dhcp;
    @FXML ChoiceBox<String> choiceType;

    String type, id, buyDate, installationDate, brand, serialNumber, model, ipv4, dns, netmask, gateway, url, comments, dhcp;

    @FXML
    private void openRootLayout() throws IOException {
        App.setRoot("rootLayout");
    }

    @FXML
    private void openCustomer() throws IOException {
        App.setRoot("customer");
    }

    @FXML
    private void openPeripheral() throws IOException {
        App.setRoot("peripheral");
    }

    @FXML
    public void initialize() {
        ObservableList<String> networkType = FXCollections.observableArrayList();
        networkType.addAll("Routeur", "Ordinateur", "Printer", "Switch");
        choiceType.setItems(networkType);
    }

    @FXML
    public void getInfo() throws IOException {
        type = choiceType.getValue();
        id = ID.getText();
        buyDate = BuyDate.getText();
        installationDate = InstallationDate.getText();
        brand = Brand.getText();
        serialNumber = SerialNumber.getText();
        model = Model.getText();
        ipv4 = Ipv4.getText();
        dns = Dns.getText();
        netmask = Netmask.getText();
        url = Url.getText();
        comments = Comments.getText();
        dhcp = Dhcp.getText();

    }

    @FXML
    public void save() throws IOException {
        getInfo();
        String type = choiceType.getValue();
        Peripheral peripheral = new Peripheral(
                id, buyDate, installationDate, brand, serialNumber, model, type);
        App.setRoot("peripheral");
    }


}
