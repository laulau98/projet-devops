package fr.b3.epsi.controller;

import java.io.IOException;
import fr.b3.epsi.model.App;
import javafx.fxml.FXML;

public class SecondaryController {

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}
