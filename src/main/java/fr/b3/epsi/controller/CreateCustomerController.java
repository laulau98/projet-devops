package fr.b3.epsi.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;


import java.io.IOException;

import fr.b3.epsi.model.App;
import fr.b3.epsi.model.Customer;

public class CreateCustomerController {

	@FXML
	TextField Name, Address, PostCode, Mail, Number;

	@FXML
	private void openRootLayout() throws IOException {
		App.setRoot("rootLayout");
	}

	@FXML
	private void openCustomer() throws IOException {
		App.setRoot("customer");
	}

	@FXML
	private void openPeripheral() throws IOException {
		App.setRoot("peripheral");
	}

	@FXML
	public void save() throws IOException {
		Customer customer = new Customer(Name.getText(), Number.getText(), Mail.getText(), Address.getText(),
				PostCode.getText());
		App.setRoot("customer");
	}

}
