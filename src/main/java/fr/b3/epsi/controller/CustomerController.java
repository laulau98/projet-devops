package fr.b3.epsi.controller;


import javafx.fxml.FXML;

import java.io.IOException;

import fr.b3.epsi.model.App;

public class CustomerController {

    @FXML
    private void openRootLayout() throws IOException {
        App.setRoot("rootLayout");
    }

    @FXML
    private void openCustomer() throws IOException {
        App.setRoot("customer");
    }

    @FXML
    private void openPeripheral() throws IOException {
        App.setRoot("peripheral");
    }

    @FXML
    private void createCustomer() throws IOException {
        App.setRoot("createCustomer");
    }
}

