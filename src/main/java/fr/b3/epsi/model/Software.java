package fr.b3.epsi.model;

public class Software extends Device{

    private String addressInstallation;
    private int version;

    public Software() {
    }

    public Software(String id, String buyDate, String installationDate, String brand, String addressInstallation, int version) {

        super(id, buyDate, installationDate, brand);
        setAddressInstallation(addressInstallation);
        setVersion(version);

    }

    public String getAddressInstallation() {
        return addressInstallation;
    }

    public void setAddressInstallation(String addressInstallation) {
        this.addressInstallation = addressInstallation;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
