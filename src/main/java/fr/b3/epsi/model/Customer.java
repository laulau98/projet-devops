package fr.b3.epsi.model;

public class Customer {

    private String nameCompany;
    private String number;
    private String mail;
    private String address;
    private String postCode;

    public Customer() {

        setNameCompany("Unknown");
        setNumber("0000000000");
        setMail("Unknown");
        setAddress("Unknown");
        setPostCode("00000000");
    }

    public Customer(String nameCompany, String number, String mail, String address, String postCode){

        setNameCompany(nameCompany);
        setNumber(number);
        setMail(mail);
        setAddress(address);
        setPostCode(postCode);
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public String getNumber() {
        return number;
    }

    public String getMail() {
        return mail;
    }

    public String getAddress() {
        return address;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }
}
