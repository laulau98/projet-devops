package fr.b3.epsi.model;

public class NetworkDevice extends Device {

    private String type;
    private String serialNumber;
    private String model;
    private boolean dhcp;
    private String dns;
    private String ipv4;
    private String gateway;
    private String netmask;
    private String url;

    public NetworkDevice() {
    }

    public NetworkDevice(String id, String buyDate, String installationDate, String brand, String type, String serialNumber, String model, String pDhcp, String dns, String ipv4, String gateway, String netmask, String url) {

        super(id, buyDate, installationDate, brand);
        this.type = type;
        this.serialNumber = serialNumber;
        this.model = model;
        if (pDhcp == "DHCP") dhcp = true;
        this.dns = dns;
        this.ipv4 = ipv4;
        this.gateway = gateway;
        this.netmask = netmask;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isDhcp() {
        return dhcp;
    }

    public void setDhcp(boolean dhcp) {
        this.dhcp = dhcp;
    }

    public String getDns() {
        return dns;
    }

    public void setDns(String dns) {
        this.dns = dns;
    }

    public String getIpv4() {
        return ipv4;
    }

    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
