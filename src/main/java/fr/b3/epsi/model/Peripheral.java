package fr.b3.epsi.model;

public class Peripheral extends Device {

    private String serialNumber;
    private String model;
    private String type;

    public Peripheral() {
    }

    public Peripheral(String id, String buyDate, String installationDate, String brand, String serialNumber, String model, String type) {

        super(id, buyDate, installationDate,brand);
        setSerialNumber(serialNumber);
        setModel(model);
        setType(type);
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
