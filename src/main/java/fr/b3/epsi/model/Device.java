package fr.b3.epsi.model;

public class Device {

    private String id;
    private String buyDate;
    private String installationDate;
    private String brand;

    public Device() {

    }

    public Device(String id, String buyDate, String installationDate, String brand) {

        setId(id);
        setBuyDate(buyDate);
        setInstallationDate(installationDate);
        setBrand(brand);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
