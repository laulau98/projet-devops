package fr.b3.epsi.model;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    private static Scene scene; 
    private Group contentRootPanel;

    @Override
    public void start(Stage stage) throws IOException {

        /*scene = new Scene(loadFXML("primary"), 640, 480);
        stage.setTitle("Test de fonctionnemnt");
        stage.setScene(scene);
        stage.show();*/

        Group root = new Group();
        scene = new Scene(root, 1050, 800);
        root.getChildren().addAll(loadFXML("rootLayout"));
        stage.setTitle("Application de Gestion");
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
