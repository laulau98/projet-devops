package fr.b3.epsi.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.b3.epsi.model.Customer;

public class CustomerTest {
	
	private Customer customer;
	
	@Test
	public void testDeLaMethodeGetNumber() {
		
		customer = new Customer();
		String number = customer.getNumber();
		String testResults = "0000000000";
		
		assertEquals(testResults, number);
	}
	
	@Test
	public void testDeLaMethodeGetMail() {
		
		customer = new Customer();
		String email = customer.getMail();
		String testResults = "Unknown";
		
		assertEquals(testResults, email);
	}
	
	@Test
	public void testDeLaMethodeGetNameCompany() {
		
		customer = new Customer();
		String name = customer.getNameCompany();
		String testResults = "Unknown";
		
		assertEquals(testResults, name);
	}
	
	@Test
	public void testDeLaMethodeGetAddress() {
		
		customer = new Customer();
		String address = customer.getAddress();
		String testResults = "Unknown";
		
		assertEquals(testResults, address);
	}
	
	@Test
	public void testDeLaMethodePostCode() {
		
		customer = new Customer();
		String postCode = customer.getPostCode();
		String testResults = "00000000";
		
		assertEquals(testResults, postCode);
	}
}
